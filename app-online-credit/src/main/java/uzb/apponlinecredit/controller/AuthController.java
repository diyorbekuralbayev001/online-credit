package uzb.apponlinecredit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import uzb.apponlinecredit.entity.User;
import uzb.apponlinecredit.payload.ApiResponse;
import uzb.apponlinecredit.payload.JwtToken;
import uzb.apponlinecredit.payload.ReqSignIn;
import uzb.apponlinecredit.payload.UserDto;
import uzb.apponlinecredit.security.CurrentUser;
import uzb.apponlinecredit.security.JwtTokenProvider;
import uzb.apponlinecredit.service.AuthService;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthService authService;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    /**
     *User login
     */
    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ReqSignIn request) {
        Authentication authentication = authenticationManager.authenticate(new
                UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtTokenProvider.generateToken(((User) authentication.getPrincipal()));
        return ResponseEntity.ok(new JwtToken(token));
    }

    /**
     *Userni register qilish uchun
     */
    @PostMapping("/registerUser")
    public HttpEntity<?> registerUser(@RequestBody UserDto userDto) {
        ApiResponse apiResponse = authService.registerUser(userDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/me")
    public HttpEntity<?> userMe(@CurrentUser User user) {
        return ResponseEntity.status(user != null ? 200 : 409).body(user);
    }
}
