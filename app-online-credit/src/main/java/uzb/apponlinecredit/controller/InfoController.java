package uzb.apponlinecredit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uzb.apponlinecredit.entity.User;
import uzb.apponlinecredit.security.CurrentUser;
import uzb.apponlinecredit.service.InfoService;

@RestController
@RequestMapping("/api/info")
public class InfoController {
    @Autowired
    InfoService infoService;

    /**
     *User ni ma'lumotlari
     */
    @GetMapping
    public HttpEntity<?> getUserInfo(@CurrentUser User user){
        return ResponseEntity.status(200).body(infoService.getUserDto(user));
    }
}
