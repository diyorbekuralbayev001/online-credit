package uzb.apponlinecredit.service;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.apponlinecredit.entity.User;
import uzb.apponlinecredit.entity.enums.Gender;
import uzb.apponlinecredit.entity.enums.RoleName;
import uzb.apponlinecredit.payload.ApiResponse;
import uzb.apponlinecredit.payload.UserDto;
import uzb.apponlinecredit.repository.RoleRepository;
import uzb.apponlinecredit.repository.UserRepository;

import java.util.UUID;
import java.util.regex.Pattern;

@Service
public class AuthService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;


    public UserDetails getUserById(UUID userId) {
        return userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("getUser"));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByPassport(username).orElseThrow(() -> new UsernameNotFoundException("Don't find username or change username"));
    }

    /**
     *User register method
     */
    public ApiResponse registerUser(UserDto userDto) {
        try {
            ApiResponse checkPassword = checkPassword(userDto);
            if (!checkPassword.isSuccess()) {
                return checkPassword;
            }
            boolean passport = Pattern.matches("[A-Z]{2}[0-9]{7}", userDto.getPassport());
            if (!(passport))
                return new ApiResponse("Error typing passport info. You must write this Example: AB1234567, but your look that " + userDto.getPassport(), false);
            userRepository.save(makeUser(userDto, false));
            return new ApiResponse("Successfully saved", true);
        } catch (Exception e) {
            return new ApiResponse("Error saving in User", false);
        }
    }

    /**
     *Tayyor User yasash uchun method
     */
    @SneakyThrows
    public User makeUser(UserDto userDto, boolean admin) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setMiddleName(userDto.getMiddleName());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setSalary(userDto.getSalary());
        user.setPassport(userDto.getPassport());
        Gender gender;
        switch (userDto.getGender()) {
            case "MEN":
                gender = Gender.MEN;
                break;
            case "WOMEN":
                gender = Gender.WOMEN;
                break;
            default:
                return null;
        }
        user.setGender(gender);
        user.setGivenBy(userDto.getGivenBy());
        user.setIssueDate(userDto.getIssueDate());
        user.setExpireDate(userDto.getExpireDate());
        user.setBirthDate(userDto.getBirthDate());
        user.setBirthOfPlace(userDto.getBirthOfPlace());
        user.setNationality(userDto.getNationality());
        user.setRoles(roleRepository.findAllByRoleName(!admin ? RoleName.ROLE_USER : RoleName.ROLE_ADMIN));
        user.setEnabled(true);
        return user;
    }

    /**
     *Parolini tekshirish uchun method
     */
    public ApiResponse checkPassword(UserDto userDto) {
        if (userDto.getPassword().length() < 6 || userDto.getPassword().length() > 10)
            return new ApiResponse("Password size is must be between 6 and 10 character!", false);
        if (!(userDto.getPassword().equals(userDto.getPrePassword())))
            return new ApiResponse("Password and pre-password is not equals!", false);
        return new ApiResponse("OK", true);
    }
}
